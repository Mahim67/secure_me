<section id="team" class="team">

    <div class="container" data-aos="fade-up">

        <header class="section-header">
            <h2>Our Team</h2>
            {{-- <p>Our hard working team</p> --}}
        </header>

        <div class="row gy-4">

            <div class="col-lg-4 col-md-6 d-flex align-items-stretch" data-aos="fade-up" data-aos-delay="100">
                <div class="member">
                    <div class="member-img">
                        <img src="{{ asset('') }}assets/frontend/img/team/apollo_das.png" class="img-fluid"
                            alt="">
                        <div class="social">
                            <a href="https://www.google.com"><i class="bi bi-google"></i></a>
                            <a href="https://www.facebook.com/apollo.das948" target="_blank"><i class="bi bi-facebook"></i></a>
                            <a href="https://l.facebook.com/l.php?u=https%3A%2F%2Finstagram.com%2F______apollo_______________%3Futm_medium%3Dcopy_link%26fbclid%3DIwAR38_LKhyYtBI-f1V8r7Mua90KAySKwUebiNQQdDx8YFG6IhO8RWgT9NYpo&h=AT2lwiP8q6dsR20u4hYu5MvO0i-mevf38WDjGJ3F0u1eq1NFUCW1vN6fU_uIv-bLZRDoJlJTwCvjqQQmh-Oc1CxhnhqxvFDGE7KTIDZz-Rj0tlr_u3lMI-S3RAwKOR_1qglwLQ"><i class="bi bi-instagram"></i></a>
                        </div>
                    </div>
                    <div class="member-info">
                        <h4>Apollo Das</h4>
                        <span>Mail : dasapollo948@gmail.com</span>
                        <p>Phone no : +8801533188750</p>
                    </div>
                </div>
            </div>

            <div class="col-lg-4 col-md-6 d-flex align-items-stretch" data-aos="fade-up" data-aos-delay="200">
                <div class="member">
                    <div class="member-img">
                        <img src="{{ asset('') }}assets/frontend/img/team/marin.png" class="img-fluid" alt="">
                        <div class="social">
                            <a href="https://www.google.com"><i class="bi bi-google"></i></a>
                            <a href="https://www.facebook.com/profile.php?id=100020327928570" target="_blank"><i class="bi bi-facebook"></i></a>
                            <a href="https://l.facebook.com/l.php?u=https%3A%2F%2Finstagram.com%2Fmarin_marufa%3Futm_medium%3Dcopy_link%26fbclid%3DIwAR2ruNWK0VAq9Q8aQC87FamNYtrqv5MlzbVZHEi42wDklBk9l0-Na4O_lJY&h=AT2lwiP8q6dsR20u4hYu5MvO0i-mevf38WDjGJ3F0u1eq1NFUCW1vN6fU_uIv-bLZRDoJlJTwCvjqQQmh-Oc1CxhnhqxvFDGE7KTIDZz-Rj0tlr_u3lMI-S3RAwKOR_1qglwLQ"><i class="bi bi-instagram"></i></a>
                        </div>
                    </div>
                    <div class="member-info">
                        <h4>Marufa Husan Marin</h4>
                        <span>Mail : Marufa.Hossain.marin@gmail.com</span>
                        <p>Phone No : +880 1790-832787</p>
                    </div>
                </div>
            </div>

            <div class="col-lg-4 col-md-6 d-flex align-items-stretch" data-aos="fade-up" data-aos-delay="300">
                <div class="member">
                    <div class="member-img">
                        <img src="{{ asset('') }}assets/frontend/img/team/tasmia.png" class="img-fluid" alt="">
                        <div class="social">
                            <a href="https://www.google.com"><i class="bi bi-google"></i></a>
                            <a href="https://www.facebook.com/profile.php?id=100011272801898" target="_blank"><i class="bi bi-facebook"></i></a>
                            <a href="https://l.facebook.com/l.php?u=https%3A%2F%2Finstagram.com%2Ftasmia_preity%3Futm_medium%3Dcopy_link%26fbclid%3DIwAR3KoNVwCx1Haf_zgZLOCbtMC4kmvFFWubDjbP81CCbGsbKd2oZlWSFO4Q0&h=AT2lwiP8q6dsR20u4hYu5MvO0i-mevf38WDjGJ3F0u1eq1NFUCW1vN6fU_uIv-bLZRDoJlJTwCvjqQQmh-Oc1CxhnhqxvFDGE7KTIDZz-Rj0tlr_u3lMI-S3RAwKOR_1qglwLQ"><i class="bi bi-instagram"></i></a>
                        </div>
                    </div>
                    <div class="member-info">
                        <h4>Tasmia kamal preity </h4>
                        <span>Mail : tasmiapriety@gmail.com</span>
                        <p>Phone No: +880 1882-818072</p>
                    </div>
                </div>
            </div>

        </div>

    </div>

</section>
